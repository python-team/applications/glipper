glipper (2.4-7) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:12:09 -0500

glipper (2.4-6) unstable; urgency=high

  * debian/control:
    - Changed Maintainer's email. (Closes: #793088).

 -- José Ernesto Dávila Pantoja <josernestodavila@gmail.com>  Wed, 22 Jul 2015 15:14:18 -0600

glipper (2.4-5) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 3.9.6.
    - Added dh-python to Build-Depends.

  [ Thanks Ernesto Domato ]
  * debian/patches:
    - Update the autostart-only-in-gnome.txt path
      to include the MATE desktop (Closes: #724599).

 -- José Ernesto Dávila Pantoja <josernestodavila@ubuntu.com>  Fri, 17 Jul 2015 16:53:57 -0600

glipper (2.4-4) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 3.9.4.

    [ Thanks Jakub Wilk <jwilk@debian.org> ]
    - Use canonical URIs for vcs-* fields.

  * debian/copyright:
    - Added Karderio to copyright.

  [ Thanks Jeremy Bicha <jbicha@ubuntu.com> ]
    - Updated help for python-distutils-extra changes (Closes: #717674).
  * debian/rules:
    - Remove the correct directory structure for help files. (Closes: #718158).
  * debian/patches:
    - Added fix-help.patch to fix the help button in Preferences dialog.
    - Added Unity to the autostart ShowInOnly line. Move patch to unstable
      version of the package.

 -- José Ernesto Dávila Pantoja <josernestodavila@ubuntu.com>  Sat, 03 Aug 2013 20:09:19 -0600

glipper (2.4-3) experimental; urgency=low

  [ Thanks Timo Weingärtner <timo@tiwe.de> ]
  * debian/patches:
    - Add autostart-only-in-gnome.txt (Closes: #691174).

 -- José Ernesto Dávila Pantoja <josernestodavila@ubuntu.com>  Sat, 17 Nov 2012 13:31:40 -0600

glipper (2.4-2) experimental; urgency=low

  [ David Prévot <taffit@debian.org> ]
  * debian/copyright:
    - Integrate changes from 2.3-3.1 (Closes: #688608).

 -- José Ernesto Dávila Pantoja <josernestodavila@ubuntu.com>  Mon, 15 Oct 2012 18:30:07 -0600

glipper (2.4-1) experimental; urgency=low

  * New upstream release. (LP: #936650)
  * Removed debian/patches directory. The patch is applied upstream.

 -- José Ernesto Dávila Pantoja <josernestodavila@ubuntu.com>  Sun, 26 Aug 2012 16:04:52 -0600

glipper (2.3-3) unstable; urgency=medium

  [ Bradley M. Froehle ]
  * debian/patches:
    - Add ctrl_c_item-is-None.patch (LP: #904367)

 -- Jose Ernesto Davila Pantoja <josernestodavila@ubuntu.com>  Tue, 19 Jun 2012 10:12:27 -0600

glipper (2.3-2) unstable; urgency=medium

  * debian/rules:
    - Removed dependency on python-gnomeapplet (Closes: #659806),
    thanks to Sebastian Ramacher <s.ramacher@gmx.at> for the patch.
  * debian/control:
    - Bump Standars-Version to 3.9.3.

 -- Jose Ernesto Davila Pantoja <josernestodavila@ubuntu.com>  Tue, 20 Mar 2012 10:09:10 -0600

glipper (2.3-1) unstable; urgency=low

  * New Maintainer (Closes: #583244).
  * New upstream release (Closes: #446463, #464897, #611256).
  * debian/rules:
    - Overrided dh_auto_clean, package wasn't cleanning properly.
    - Overrided dh_gencontrol.
    - Overrided dh_auto_install, to remove empty directories being installed.
  * Added debian/source/options, to ignore .pot file updated on build time.
  * debian/control:
    - Added ${dist:Depends} to generate different Depends for Debian/Ubuntu.
    - Build-dep on debhelper (>= 8).
    - Added X-Python-Version field for the source package.
  * debian/compat:
    + Update to debhelper 8.
  * debian/copyright:
    - Modified to use DEP-5 format.

  [ Laszlo Pandy ]

  * debian/watch:
    - Updated to new download location on launchpad.net.
  * debian/rules:
    + Remove all rules and replace with debhelper template because
    upstream build system has changed.
  * debian/docs:
    - Remove TODO and NEWS; they no longer exist upstream.
  * debian/control:
    - Bump Standars-Version to 3.9.2
    - Change Homepage to new launchpad.net project (Closes: #615315).
    - Update build dependencies for new upstream build system
    (python-distutils). (Closes: #558552)
    - Add python-xdg dependency.
    - Remove dependency on python-glade2 (Closes: #551459).

 -- Jose Ernesto Davila Pantoja <josernestodavila@ubuntu.com>  Fri, 20 Jan 2012 10:19:18 -0600

glipper (1.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control:
    - (Build-)depend on python-gnomeapplet (Closes: #537032).
    - Also depend on python-gobject.
    - Do not (build-)depend on python-gnome2-extras(-dev), both packages
      are scheduled for removal soon (Closes: #485311).

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 03 Sep 2009 20:35:10 +0200

glipper (1.0-1) unstable; urgency=low

  * New upstream release.
    - complete rewrite of the whole application, now it's a
      GNOME panel applet written in python
    - new dependencies on: python, python-support, gconf2,
      python-gnome2, python-gnome2-extras, gnome-panel
  * New Maintainer. (Closes: #436550)
    - 01_missing-headers.dpatch: add missing license headers from CVS
    - add debian/glipper.NEWS

 -- Davide Truffa <davide@catoblepa.org>  Thu, 11 Oct 2007 03:32:06 +0200

glipper (0.95.1-3) unstable; urgency=low

  * Create editable XML for the manpage to allow easier
  updates (courtesy of doclifter).
  * Update manpage with a tip on using glipper preferences
  to solve issues with pasting images.

 -- Neil Williams <codehelp@debian.org>  Sat, 14 Apr 2007 21:13:18 +0100

glipper (0.95.1-2) unstable; urgency=low

  * Added Simple Chinese translation provided by
  Liu ZhuYuan <liu.zhuyuan@gd-linux.com> as a Debian patch
  until incorporated upstream.

 -- Neil Williams <codehelp@debian.org>  Mon, 15 Jan 2007 14:04:16 +0000

glipper (0.95.1-1) unstable; urgency=low

  * New upstream release.

 -- Neil Williams <codehelp@debian.org>  Tue, 21 Nov 2006 22:19:18 +0000

glipper (0.95-1) unstable; urgency=low

  * New upstream release

 -- Neil Williams <codehelp@debian.org>  Mon, 13 Nov 2006 17:37:40 +0000

glipper (0.89-1) unstable; urgency=low

  * Initial release. (Closes: #387426: ITP: glipper -- A
    clipboardmanager for GNOME and other window managers - Debian Bug
    report logs)
  - add menu file and .xpm version of glipper.png
  - add manpage.

 -- Neil Williams <linux@codehelp.co.uk>  Thu, 14 Sep 2006 15:18:28 +0100

